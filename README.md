[![CircleCI](https://circleci.com/bb/herf/yoe.svg?style=shield)](https://circleci.com/bb/herf/yoe)
[![codecov.io](https://codecov.io/bb/herf/yoe/coverage.svg?branch=master)](https://codecov.io/bb/herf/yoe?branch=master)
[![Bitbucket issues](https://img.shields.io/bitbucket/issues/herf/yoe)](https://bitbucket.org/herf/memory-manager/issues)

A simple 3D engine written in C++11
