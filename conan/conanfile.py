from conans import ConanFile, CMake, tools
import os


def _get_package_version():
    try:
        # Check if we are running in a CircleCI tagged pipeline
        return os.environ["CIRCLE_TAG"]
    except KeyError:
        return "dev"


class YoeConan(ConanFile):
    name = "yoe"
    version = _get_package_version()
    license = "<Put the package license here>"
    author = "Rudolf Heszele rudolf.heszele@gmail.com"
    url = "https://bitbucket.org/herf/yoe/src/master/"
    description = "Simple game engine written in C++11"
    topics = ("3D", "render", "game", "engine")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False}
    requires = "glm/0.9.9.5@g-truc/stable"
    generators = "cmake"

    def source(self):
        self.run("git clone -b ci https://herf@bitbucket.org/herf/yoe.git")
        # This small hack might be useful to guarantee proper /MT /MD linkage
        # in MSVC if the packaged project doesn't have variables to set it
        # properly
        tools.replace_in_file("yoe/CMakeLists.txt", "project(yoe)",
                              '''project(yoe)
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()''')

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="yoe", args=["-DYOE_BUILD_TESTS=OFF"])
        cmake.build()
        # Explicit way:
        # self.run('cmake %s/hello %s'
        #          % (self.source_folder, cmake.command_line))
        # self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        self.copy("*.hh", dst="include", src="yoe/include")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.dylib*", dst="lib", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["yoe"]
