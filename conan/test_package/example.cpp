#include <iostream>
#include <cassert>
#include <yoe/world.hh>
#include <yoe/actor.hh>
#include <yoe/component.hh>

class test_actor_type: public yoe::actor_type
{
public:
    test_actor_type(int i): _i(i)
    {
        // Nothing to do yet
    }

    int get_i() const
    {
        return _i;
    }

private:
    int _i;
};

class test_component_type: public yoe::component_type
{
public:
    test_component_type(float f): _f(f)
    {
        // Nothing to do yet
    }

    float get_f() const
    {
        return _f;
    }

private:
    float _f;
};

class random_component_type: public yoe::component_type
{

};

int main()
{
    {
        yoe::smart_pointer_type<test_actor_type> actor = yoe::world_type::get_instance().create<test_actor_type>(5);
        yoe::smart_pointer_type<test_component_type> component = actor->create_component<test_component_type>(6.0f);

        assert(actor.is_valid());
        assert(component.is_valid());
        assert(actor->get_i() == 5);
        assert(actor->get_components().size() == 1);
        assert(actor->get_component<yoe::component_type>() == component.cast<yoe::component_type>());
        assert(actor->get_component<test_component_type>() == component);
        assert(actor->get_component<random_component_type>() == yoe::smart_pointer_type<random_component_type>());
        assert(component->get_f() == 6.0f);
        assert(component->get_parent() == actor.cast<yoe::actor_type>());
    }

    std::cout << "Tests OK" << std::endl;

    return 0;
}
