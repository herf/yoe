#include "yoe_tests.hh"

SCENARIO("actor_type objects can be easily used")
{
    GIVEN("A custom actor_type object created by world_type")
    {
        yoe::smart_pointer_type<test_actor_type> actor = yoe::world_type::get_instance().create<test_actor_type>(5, 6.0f);

        THEN("This actor_type object is a valid, working object")
        {
            REQUIRE(actor.is_valid());
            REQUIRE(actor);
            CHECK(actor->get_i() == 5);
            CHECK(actor->get_f() == 6.0f);
            CHECK(actor->get_components().empty());
            AND_THEN("Components can be added to thie actor_type object")
            {
                class unknown_component_type: public yoe::component_type
                {
                    
                };
                yoe::smart_pointer_type<test_component_type> component = actor->create_component<test_component_type>(50, 60.0f);

                REQUIRE(component.is_valid());
                REQUIRE(component);
                CHECK(component->get_i() == 50);
                CHECK(component->get_f() == 60.0f);
                REQUIRE(actor->get_components().size() == 1);
                CHECK(actor->get_components()[0] == component.cast<yoe::component_type>());
                CHECK(actor->get_component<test_component_type>() == component);
                CHECK(actor->get_component<unknown_component_type>() == yoe::smart_pointer_type<unknown_component_type>());
                CHECK(actor->get_components<test_component_type>().size() == 1);
                CHECK(actor->get_components<unknown_component_type>().empty());
                AND_THEN("When the actor destroyed, all of its components are also destroyed")
                {
                    actor.destroy();
                    CHECK_FALSE(actor.is_valid());
                    CHECK_FALSE(actor);
                    CHECK_FALSE(component.is_valid());
                    CHECK_FALSE(component);
                }
            }
        }
    }
}
