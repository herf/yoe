#include "yoe_tests.hh"
#include "test_object.hh"
#include <yoe/smart_pointer.hh>
#include <yoe/world.hh>

SCENARIO("smart_pointer_type objects can be easily used")
{
    GIVEN("A smart_pointer_type object, create by world_type")
    {
        destructor_callback_implementation_type destructor_callback_a;
        yoe::smart_pointer_type<test_object_type> test_a = yoe::world_type::get_instance().create<test_object_type>(5, 6.0f, &destructor_callback_a);

        REQUIRE(test_a.is_valid());
        REQUIRE(test_a);
        CHECK(test_a->get_i() == 5);
        CHECK(test_a->get_f() == 6.0f);
        THEN("This object can be copied into an other smart_pointer_typeobject")
        {
            yoe::smart_pointer_type<test_object_type> test_b = test_a;

            REQUIRE(test_a.is_valid());
            REQUIRE(test_a);
            REQUIRE(test_b.is_valid());
            REQUIRE(test_b);
            CHECK(test_a == test_b);
            CHECK(test_a->get_i() == 5);
            CHECK(test_a->get_f() == 6.0f);
            CHECK(test_b->get_i() == 5);
            CHECK(test_b->get_f() == 6.0f);
            AND_THEN("Given a third smart_pointer_type object created by wold_type")
            {
                destructor_callback_implementation_type destructor_callback_b;
                yoe::smart_pointer_type<test_object_child_type> test_c = yoe::world_type::get_instance().create<test_object_child_type>(50, 60.0f, 70.0, &destructor_callback_b);

                REQUIRE(test_c.is_valid());
                REQUIRE(test_c);
                CHECK(test_c->get_i() == 50);
                CHECK(test_c->get_f() == 60.0f);
                CHECK(test_c->get_d() == 70.0f);
                THEN("The this third smart_pointer_type object can be cast to other related smart_pointer_type objects")
                {
                    yoe::smart_pointer_type<yoe::object_type> test_d = test_c.cast<yoe::object_type>();
                    yoe::smart_pointer_type<test_object_type> test_e = test_c.cast<test_object_type>();
                    // This will not compile, since unrelated_test_object_type, has nothing to to with test_object_child_type
                    // yoe::smart_pointer_type<unrelated_test_object_type> test_f = test_c.cast<unrelated_test_object_type>();

                    REQUIRE(test_d.is_valid());
                    REQUIRE(test_d);
                    REQUIRE(test_e.is_valid());
                    REQUIRE(test_e);
                    REQUIRE(test_c.is_valid());
                    REQUIRE(test_c);
                    CHECK(test_c->get_i() == 50);
                    CHECK(test_c->get_f() == 60.0f);
                    CHECK(test_c->get_d() == 70.0f);
                    CHECK(test_e->get_i() == 50);
                    CHECK(test_e->get_f() == 60.0f);
                    AND_THEN("Destroyed smart_pointer_type objects will not destroy the actual object if there are other pointers out there")
                    {
                        {
                            yoe::smart_pointer_type<test_object_type> test_f = test_c.cast<test_object_type>();

                            REQUIRE(test_f.is_valid());
                            REQUIRE(test_f);
                            CHECK(test_f->get_i() == 50);
                            CHECK(test_f->get_f() == 60.0f);
                        }
                        CHECK_FALSE(destructor_callback_b.is_destructor_called());
                        CHECK(destructor_callback_b.get_number_of_destructor_calls() == 0);
                        CHECK(test_c.is_valid());
                        CHECK(test_c);
                        AND_THEN("All the objects can be easily and safely destroyed")
                        {
                            test_a.destroy();
                            CHECK(destructor_callback_a.is_destructor_called());
                            CHECK(destructor_callback_a.get_number_of_destructor_calls() == 1);
                            CHECK(destructor_callback_b.get_number_of_destructor_calls() == 0);
                            CHECK_FALSE(destructor_callback_b.is_destructor_called());
                            CHECK_FALSE(test_a.is_valid());
                            CHECK_FALSE(test_a);
                            CHECK_FALSE(test_b.is_valid());
                            CHECK_FALSE(test_b);
                            CHECK(test_c.is_valid());
                            CHECK(test_c);
                            CHECK(test_d.is_valid());
                            CHECK(test_d);
                            CHECK(test_e.is_valid());
                            CHECK(test_e);
                            // Calling destroy twice should not cause any trouble
                            test_a.destroy();
                            CHECK_FALSE(destructor_callback_b.is_destructor_called());
                            CHECK(destructor_callback_b.get_number_of_destructor_calls() == 0);
                            CHECK(destructor_callback_a.get_number_of_destructor_calls() == 1);
                            CHECK_FALSE(test_a.is_valid());
                            CHECK_FALSE(test_a);
                            CHECK_FALSE(test_b.is_valid());
                            CHECK_FALSE(test_b);
                            CHECK(test_c.is_valid());
                            CHECK(test_c);
                            CHECK(test_d.is_valid());
                            CHECK(test_d);
                            CHECK(test_e.is_valid());
                            CHECK(test_e);
                            // Calling destroy another smart_pointer_type object, referencing to an already deleted object
                            // should not cause any trouble
                            test_b.destroy();
                            CHECK_FALSE(destructor_callback_b.is_destructor_called());
                            CHECK(destructor_callback_b.get_number_of_destructor_calls() == 0);
                            CHECK(destructor_callback_a.get_number_of_destructor_calls() == 1);
                            CHECK_FALSE(test_a.is_valid());
                            CHECK_FALSE(test_a);
                            CHECK_FALSE(test_b.is_valid());
                            CHECK_FALSE(test_b);
                            CHECK(test_c.is_valid());
                            CHECK(test_c);
                            CHECK(test_d.is_valid());
                            CHECK(test_d);
                            CHECK(test_e.is_valid());
                            CHECK(test_e);
                            test_d.destroy();
                            CHECK(destructor_callback_b.is_destructor_called());
                            CHECK(destructor_callback_b.get_number_of_destructor_calls() == 1);
                            CHECK(destructor_callback_a.get_number_of_destructor_calls() == 1);
                            CHECK_FALSE(test_a.is_valid());
                            CHECK_FALSE(test_a);
                            CHECK_FALSE(test_b.is_valid());
                            CHECK_FALSE(test_b);
                            CHECK_FALSE(test_c.is_valid());
                            CHECK_FALSE(test_c);
                            CHECK_FALSE(test_d.is_valid());
                            CHECK_FALSE(test_d);
                            CHECK_FALSE(test_e.is_valid());
                            CHECK_FALSE(test_e);
                        }
                    }
                }
            }
        }
    }
}
