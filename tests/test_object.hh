#pragma once
#include <string>
#include <utility>
#include <yoe/object.hh>
#include <yoe/actor.hh>
#include <yoe/component.hh>

class test_object_type;

class destructor_callback_type
{
public:
    virtual ~destructor_callback_type() = default;
    virtual void on_destructor_called(test_object_type* test) = 0;
};

class test_object_type: public yoe::object_type
{
public:
    test_object_type(int i, float f, destructor_callback_type* destructor_callback):
        _i(i), _f(f),
        _destructor_callback(destructor_callback)
    {
        // Nothing to do yet
    }

    ~test_object_type()
    {
        if(nullptr != _destructor_callback)
        {
            _destructor_callback->on_destructor_called(this);
        }
    }

    int get_i() const
    {
        return _i;
    }

    float get_f() const
    {
        return _f;
    }

private:
    int _i;
    float _f;
    destructor_callback_type* _destructor_callback;
};

class test_object_child_type: public test_object_type
{
public:
    test_object_child_type(int i, float f, double d, destructor_callback_type* destructor_callback):
        test_object_type(i, f, destructor_callback),
        _d(d)
    {
        // Nothing to do yet
    }

    double get_d() const
    {
        return _d;
    }

private:
    double _d;
};

class unrelated_test_object_type: public yoe::object_type
{
public:
    unrelated_test_object_type(std::string name):
        _name(std::move(name))
    {
        // Nothing to do yet
    }

    const std::string& get_name() const
    {
        return _name;
    }

private:
    std::string _name;
};

class destructor_callback_implementation_type: public destructor_callback_type
{
public:
    virtual void on_destructor_called(test_object_type* test) override
    {
        // TODO: Would be great to test if the destructor is called with the right object
        _is_destructor_called = true;
        ++_number_of_destructor_calls;
    }

    bool is_destructor_called() const
    {
        return _is_destructor_called;
    }

    uint32_t get_number_of_destructor_calls() const
    {
        return _number_of_destructor_calls;
    }

private:
    bool _is_destructor_called = false;
    uint32_t _number_of_destructor_calls = 0;
};

// TODO: Create a general test_type class template, which inherits from the template argument
class test_actor_type: public yoe::actor_type
{
public:
    test_actor_type(int i, float f):
        _i(i),
        _f(f)
    {
        // Nothing to do yet
    }

    int get_i() const
    {
        return _i;
    }

    float get_f() const
    {
        return _f;
    }

    test_actor_type* get_this()
    {
        return this;
    }

private:
    int _i;
    float _f;
};

class test_component_type: public yoe::component_type
{
public:
    test_component_type(int i, float f):
        _i(i),
        _f(f)
    {
        // Nothing to do yet
    }

    test_component_type(parent_type parent, int i, float f):
        yoe::component_type(std::move(parent)),
        _i(i),
        _f(f)
    {
        // Nothing to do yet
    }

    int get_i() const
    {
        return _i;
    }

    float get_f() const
    {
        return _f;
    }

private:
    int _i;
    float _f;
};
