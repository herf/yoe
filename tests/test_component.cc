#include "yoe_tests.hh"
#include <yoe/world.hh>
#include <yoe/actor.hh>
#include <yoe/component.hh>

SCENARIO("component_type objects can be easily used")
{
    GIVEN("A custom component_type object")
    {
        yoe::smart_pointer_type<test_component_type> component_a = yoe::world_type::get_instance().create<test_component_type>(5, 6.0f);

        THEN("This component_type object can be used")
        {

            REQUIRE(component_a.is_valid());
            CHECK(component_a->get_i() == 5);
            CHECK(component_a->get_f() == 6.0f);
            CHECK(component_a->get_parent() == yoe::smart_pointer_type<yoe::actor_type>());
            AND_THEN("It can be destroyed")
            {
                component_a.destroy();
                CHECK_FALSE(component_a.is_valid());
            }
        }
    }
    GIVEN("A custom actor_type objects")
    {
        yoe::smart_pointer_type<test_actor_type> actor = yoe::world_type::get_instance().create<test_actor_type>(1, 2.0f);

        AND_GIVEN("A custom component_type object, with the actor_type object as its parent")
        {
        // TODO: Should be able to explicitly cast between smart_pointer_types?
            yoe::smart_pointer_type<test_component_type> component_b = yoe::world_type::get_instance().create<test_component_type>(actor.cast<yoe::actor_type>(), 5, 6.0f);

            THEN("The component_type object can be used, and it stores its parent")
            {
                REQUIRE(actor.is_valid());
                REQUIRE(component_b.is_valid());
                CHECK(component_b->get_i() == 5);
                CHECK(component_b->get_f() == 6.0f);
                CHECK(component_b->get_parent() == actor.cast<yoe::actor_type>());
                component_b.destroy();
                CHECK_FALSE(component_b.is_valid());
                actor.destroy();
                CHECK_FALSE(actor.is_valid());
            }
        }
    }
}
