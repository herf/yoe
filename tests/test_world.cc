#include "yoe_tests.hh"
#include "test_object.hh"
#include <yoe/world.hh>
#include <yoe/smart_pointer.hh>

SCENARIO("world_type objects can be used to construct object who's type is based on object_type")
{
    GIVEN("A smart_pointer_type object created with world_type")
    {
        destructor_callback_implementation_type destructor_callback;
        yoe::smart_pointer_type<test_object_type> test_a = yoe::world_type::get_instance().create<test_object_type>(5, 6.0f, &destructor_callback);

        THEN("This smart_pointer_type object is ready to be used")
        {
            yoe::smart_pointer_type<test_object_type> test_b = test_a;

            REQUIRE(test_a.is_valid());
            REQUIRE(test_a);
            REQUIRE(test_b.is_valid());
            REQUIRE(test_b);
            CHECK(test_a->get_i() == 5);
            CHECK(test_a->get_f() == 6.0f);
            CHECK(test_b->get_i() == 5);
            CHECK(test_b->get_f() == 6.0f);
            AND_THEN("The smart_pointer_type objects can be deleted using world_type")
            {
                yoe::world_type::get_instance().destroy(test_a.cast<yoe::object_type>());
                CHECK(destructor_callback.is_destructor_called());
                CHECK(destructor_callback.get_number_of_destructor_calls() == 1);
                CHECK_FALSE(test_a.is_valid());
                CHECK_FALSE(test_a);
                CHECK_FALSE(test_b.is_valid());
                CHECK_FALSE(test_b);
                // Calling destroy twice should not cause any trouble
                yoe::world_type::get_instance().destroy(test_a.cast<yoe::object_type>());
                CHECK(destructor_callback.get_number_of_destructor_calls() == 1);
                CHECK_FALSE(test_a.is_valid());
                CHECK_FALSE(test_a);
                CHECK_FALSE(test_b.is_valid());
                CHECK_FALSE(test_b);
                // Calling destroy another smart_pointer_type object, referencing to an already deleted object
                // should not cause any trouble
                yoe::world_type::get_instance().destroy(test_b.cast<yoe::object_type>());
                CHECK(destructor_callback.get_number_of_destructor_calls() == 1);
                CHECK_FALSE(test_a.is_valid());
                CHECK_FALSE(test_a);
                CHECK_FALSE(test_b.is_valid());
                CHECK_FALSE(test_b);
            }
        }
    }
}

SCENARIO("world_type will clean up undestroyed objects")
{
    GIVEN("A smart_pointer_type object created with world")
    {
        yoe::smart_pointer_type<test_actor_type> actor = yoe::world_type::get_instance().create<test_actor_type>(5, 6.0f);
        yoe::smart_pointer_type<test_component_type> component = actor->create_component<test_component_type>(50, 60.0f);
    }
    THEN("Not explicitly destroying the smart pointer will not cause any trouble")
    {
        // Nothing to do
    }
}

SCENARIO("world_type can return smart_pointer_type for raw pointers if they are 'registered'")
{
    GIVEN("An object is created with world_type::create")
    {
        yoe::smart_pointer_type<test_actor_type> actor = yoe::world_type::get_instance().create<test_actor_type>(5, 6.0f);

        THEN("A raw pointer to the created object can be turned to a smart_pointer")
        {
            yoe::smart_pointer_type<test_actor_type> new_actor = yoe::world_type::get_instance().get_smart_pointer(actor->get_this());

            CHECK(new_actor == actor);

            AND_THEN("For 'unregistered' raw pointers it will return an empty smart_pointer_type object")
            {
                test_actor_type* raw_actor = nullptr;
                yoe::smart_pointer_type<test_actor_type> unknown_actor = yoe::world_type::get_instance().get_smart_pointer(raw_actor);

                CHECK(unknown_actor == yoe::smart_pointer_type<test_actor_type>());
            }
        }
    }
}
