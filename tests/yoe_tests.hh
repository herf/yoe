#pragma once
#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
// #include <glm/gtx/quaternion.hpp>
#include <yoe/vector.hh>
#include <yoe/smart_pointer.hh>
#include "test_object.hh"

/*inline std::ostream& operator<<(std::ostream& stream, const glm::vec3& vector)
{
    stream << "[" << vector.x << ", " << vector.y << ", " << vector.z << "]";

    return stream;
}*/

inline std::ostream& operator<<(std::ostream& stream, const yoe::vector3_type& vector)
{
    stream << "[" << vector.get_x() << ", " << vector.get_y() << ", " << vector.get_z() << "]";

    return stream;
}

template<typename value_type>
inline std::ostream& operator<<(std::ostream& stream, const yoe::smart_pointer_type<value_type>& pointer)
{
    // A little hack to get the raw pointer stored in the smart_pointer_type
    stream << pointer.operator->();

    return stream;
}

/*inline std::ostream& operator<<(std::ostream& stream, const glm::quat& quaternion)
{
    stream << "[" << quaternion.x << ", " << quaternion.y << ", " << quaternion.z << ", " << quaternion.w << std::endl;

    return stream;
}*/

// Catch needs to be included after the above streaming operator defined
#include <catch2/catch.hpp>


namespace Catch
{
    // Template specialization for Catch::compareEqual
    // to allow slight differences between the vectors
    template<>
    inline bool compareEqual<yoe::vector3_type, yoe::vector3_type>(const yoe::vector3_type& lhs, const yoe::vector3_type& rhs)
    {
        return lhs.distance(rhs) < 0.000001f;
    }

    template<>
    inline bool compareEqual<yoe::smart_pointer_type<test_object_type>, yoe::smart_pointer_type<test_object_type>>(const yoe::smart_pointer_type<test_object_type>& lhs, const yoe::smart_pointer_type<test_object_type>& rhs)
    {
        return lhs == rhs;
    }
}
