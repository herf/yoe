#include "yoe_tests.hh"
#include <yoe/transform.hh>
#include <yoe/operators.hh>

SCENARIO("transform_type objects can be constructed and get their members")
{
    GIVEN("A default constructed transform_type object")
    {
        const yoe::transform_type transform;

        THEN("This object can be used to get its members")
        {
            REQUIRE(transform.get_local_position() == yoe::vector3_type(0.0f));
            REQUIRE(transform.get_world_position() == yoe::vector3_type(0.0f));
        }
        AND_THEN("This object can be used to transform coordinates between world and local space")
        {
            REQUIRE(transform.as_world_position(yoe::vector3_type(1.0f, 1.0f, 1.0f)) == yoe::vector3_type(1.0f, 1.0f, 1.0f));
            REQUIRE(transform.as_local_position(yoe::vector3_type(1.0f, 1.0f, 1.0f)) == yoe::vector3_type(1.0f, 1.0f, 1.0f));
        }
    }
    GIVEN("A non-default constructed transform_type object with a position")
    {
        const yoe::transform_type transform(yoe::vector3_type(1.0f, 1.0f, 1.0f));

        THEN("This object can be used to get its members")
        {
            REQUIRE(transform.get_local_position() == yoe::vector3_type(1.0f, 1.0f, 1.0f));
            REQUIRE(transform.get_world_position() == yoe::vector3_type(1.0f, 1.0f, 1.0f));
        }
        AND_THEN("This object can be used to transform coordinates between world and local space")
        {
            REQUIRE(transform.as_world_position(yoe::vector3_type(1.0f, 1.0f, 1.0f)) == yoe::vector3_type(2.0f, 2.0f, 2.0f));
            REQUIRE(transform.as_local_position(yoe::vector3_type(0.0f, 0.0f, 0.0f)) == yoe::vector3_type(-1.0f, -1.0f, -1.0f));
        }
    }
    GIVEN("A non-default constructed transform_type object with a scale")
    {
        const yoe::transform_type transform(yoe::vector3_type(1.0f, 1.0f, 1.0f),
                                            yoe::vector3_type(2.0f));

        THEN("This object can be used to get its members")
        {
            REQUIRE(transform.get_local_position() == yoe::vector3_type(1.0f, 1.0f, 1.0f));
            REQUIRE(transform.get_world_position() == yoe::vector3_type(1.0f, 1.0f, 1.0f));
        }
        AND_THEN("This object can be used to transform coordinates between world and local space")
        {
            REQUIRE(transform.as_world_position(yoe::vector3_type(1.0f, 1.0f, 1.0f)) == yoe::vector3_type(3.0f, 3.0f, 3.0f));
            REQUIRE(transform.as_local_position(yoe::vector3_type(0.0f, 0.0f, 0.0f)) == yoe::vector3_type(-0.5f, -0.5f, -0.5f));
        }
    }
    GIVEN("A non-default constructed transform_type object with a position and a rotation")
    {
        const yoe::transform_type transform(yoe::vector3_type(1.0f, 1.0f, 1.0f),
                                            yoe::vector3_type(1.0f),
                                            yoe::vector3_type(0.0f, 0.0f, 90.0f));

        THEN("This object can be used to get its members")
        {
            REQUIRE(transform.get_local_position() == yoe::vector3_type(1.0f, 1.0f, 1.0f));
            REQUIRE(transform.get_world_position() == yoe::vector3_type(1.0f, 1.0f, 1.0f));
        }
        AND_THEN("This object can be used to transform coordinates between world and local space")
        {
            REQUIRE(transform.as_world_position(yoe::vector3_type(1.0f, 1.0f, 1.0f)) == yoe::vector3_type(0.0f, 2.0f, 2.0f));
            REQUIRE(transform.as_local_position(yoe::vector3_type(2.0f, 2.0f, 2.0f)) == yoe::vector3_type(1.0f, -1.0f, 1.0f));
        }
    }
    GIVEN("A non-default constructed transform_type object with a position, scale and a rotation")
    {
        const yoe::transform_type transform(yoe::vector3_type(1.0f, 1.0f, 1.0f),
                                            yoe::vector3_type(2.0f),
                                            yoe::vector3_type(0.0f, 0.0f, 90.0f));

        THEN("This object can be used to get its members")
        {
            REQUIRE(transform.get_local_position() == yoe::vector3_type(1.0f, 1.0f, 1.0f));
            REQUIRE(transform.get_world_position() == yoe::vector3_type(1.0f, 1.0f, 1.0f));
        }
        AND_THEN("This object can be used to transform coordinates between world and local space")
        {
            REQUIRE(transform.as_world_position(yoe::vector3_type(1.0f, 1.0f, 1.0f)) == yoe::vector3_type(-1.0f, 3.0f, 3.0f));
            REQUIRE(transform.as_local_position(yoe::vector3_type(2.0f, 2.0f, 2.0f)) == yoe::vector3_type(0.5f, -0.5f, 0.5f));
        }
    }
}

SCENARIO("transform_type objects can have parents")
{
    GIVEN("A transform_type object with only a position")
    {
        const yoe::transform_type parent(yoe::vector3_type(1.0f, 1.0f, 1.0f));

        AND_GIVEN("Another transform_type object with only a position as the child of the previous one")
        {
            yoe::transform_type child(yoe::vector3_type(1.0f, 1.0f, 1.0f),
                                      yoe::vector3_type(1.0f),
                                      yoe::vector3_type(0.0f),
                                      &parent);

            THEN("The child transform_type object will return its members correctly")
            {
                REQUIRE(child.get_local_position() == yoe::vector3_type(1.0f, 1.0f, 1.0f));
                REQUIRE(child.get_world_position() == yoe::vector3_type(2.0f, 2.0f, 2.0f));
            }
            AND_THEN("The child transform can be used to transform coordinates between world and local space")
            {
                REQUIRE(child.as_world_position(yoe::vector3_type(1.0f, 1.0f, 1.0f)) == yoe::vector3_type(3.0f, 3.0f, 3.0f));
                REQUIRE(child.as_local_position(yoe::vector3_type(0.0f, 0.0f, 0.0f)) == yoe::vector3_type(-2.0f, -2.0f, -2.0f));
            }
            AND_THEN("The parent can be deleted, and the child will act accordingly")
            {
                child.set_parent(nullptr);

                REQUIRE(child.get_local_position() == yoe::vector3_type(1.0f, 1.0f, 1.0f));
                REQUIRE(child.get_world_position() == yoe::vector3_type(1.0f, 1.0f, 1.0f));
                REQUIRE(child.as_world_position(yoe::vector3_type(1.0f, 1.0f, 1.0f)) == yoe::vector3_type(2.0f, 2.0f, 2.0f));
                REQUIRE(child.as_local_position(yoe::vector3_type(0.0f, 0.0f, 0.0f)) == yoe::vector3_type(-1.0f, -1.0f, -1.0f));
            }
        }
    }
    GIVEN("A transform_type object with position and scale")
    {
        const yoe::transform_type parent(yoe::vector3_type(1.0f, 1.0f, 1.0f),
                                         yoe::vector3_type(2.0f));

        AND_GIVEN("Another transform_type object with only a position as the child of the previous one")
        {
            const yoe::transform_type child(yoe::vector3_type(1.0f, 1.0f, 1.0f),
                                            yoe::vector3_type(1.0f),
                                            yoe::vector3_type(0.0f),
                                            &parent);

            THEN("The child transform_type object will return its members correctly")
            {
                REQUIRE(child.get_local_position() == yoe::vector3_type(1.0f, 1.0f, 1.0f));
                REQUIRE(child.get_world_position() == yoe::vector3_type(3.0f, 3.0f, 3.0f));
            }
            AND_THEN("The child transform can be used to transform coordinates between world and local space")
            {
                REQUIRE(child.as_world_position(yoe::vector3_type(1.0f, 1.0f, 1.0f)) == yoe::vector3_type(5.0f, 5.0f, 5.0f));
                REQUIRE(child.as_local_position(yoe::vector3_type(0.0f, 0.0f, 0.0f)) == yoe::vector3_type(-1.5f, -1.5f, -1.5f));
            }
        }
    }
    GIVEN("A transform_type object with position, scale and rotation")
    {
        const yoe::transform_type parent(yoe::vector3_type(1.0f, 1.0f, 1.0f),
                                         yoe::vector3_type(2.0f),
                                         yoe::vector3_type(0.0f, 0.0f, 90.0f));

        AND_GIVEN("Another transform_type object with only a position as the child of the previous one")
        {
            const yoe::transform_type child(yoe::vector3_type(1.0f, 1.0f, 1.0f),
                                            yoe::vector3_type(1.0f),
                                            yoe::vector3_type(0.0f),
                                            &parent);

            THEN("The child transform_type object will return its members correctly")
            {
                REQUIRE(child.get_local_position() == yoe::vector3_type(1.0f, 1.0f, 1.0f));
                REQUIRE(child.get_world_position() == yoe::vector3_type(-1.0f, 3.0f, 3.0f));
            }
            AND_THEN("The child transform can be used to transform coordinates between world and local space")
            {
                REQUIRE(child.as_world_position(yoe::vector3_type(1.0f, 1.0f, 1.0f)) == yoe::vector3_type(-3.0f, 5.0f, 5.0f));
                REQUIRE(child.as_local_position(yoe::vector3_type(0.0f, 0.0f, 0.0f)) == yoe::vector3_type(-1.5f, -0.5f, -1.5f));
            }
        }
    }
    GIVEN("A transform_type object with position, scale and rotation")
    {
        const yoe::transform_type parent(yoe::vector3_type(1.0f, 1.0f, 1.0f),
                                         yoe::vector3_type(2.0f),
                                         yoe::vector3_type(0.0f, 0.0f, 90.0f));

        AND_GIVEN("Another transform_type object with position and scale as the child of the previous one")
        {
            const yoe::transform_type child(yoe::vector3_type(1.0f, 1.0f, 1.0f),
                                            yoe::vector3_type(2.0f),
                                            yoe::vector3_type(0.0f),
                                            &parent);

            THEN("The child transform_type object will return its members correctly")
            {
                REQUIRE(child.get_local_position() == yoe::vector3_type(1.0f, 1.0f, 1.0f));
                REQUIRE(child.get_world_position() == yoe::vector3_type(-1.0f, 3.0f, 3.0f));
            }
            AND_THEN("The child transform can be used to transform coordinates between world and local space")
            {
                REQUIRE(child.as_world_position(yoe::vector3_type(1.0f, 1.0f, 1.0f)) == yoe::vector3_type(-5.0f, 7.0f, 7.0f));
                REQUIRE(child.as_local_position(yoe::vector3_type(0.0f, 0.0f, 0.0f)) == yoe::vector3_type(-0.75f, -0.25f, -0.75f));
            }
        }
    }
    GIVEN("A transform_type object with position, scale and rotation")
    {
        const yoe::transform_type parent(yoe::vector3_type(1.0f, 1.0f, 1.0f),
                                         yoe::vector3_type(2.0f),
                                         yoe::vector3_type(0.0f, 0.0f, 90.0f));

        AND_GIVEN("Another transform_type object with position, scale and rotation as the child of the previous one")
        {
            const yoe::transform_type child(yoe::vector3_type(1.0f, 1.0f, 1.0f),
                                            yoe::vector3_type(2.0f),
                                            yoe::vector3_type(0.0f, 0.0f, 90.0f),
                                            &parent);

            THEN("The child transform_type object will return its members correctly")
            {
                REQUIRE(child.get_local_position() == yoe::vector3_type(1.0f, 1.0f, 1.0f));
                REQUIRE(child.get_world_position() == yoe::vector3_type(-1.0f, 3.0f, 3.0f));
            }
            AND_THEN("The child transform can be used to transform coordinates between world and local space")
            {
                REQUIRE(child.as_world_position(yoe::vector3_type(1.0f, 1.0f, 1.0f)) == yoe::vector3_type(-5.0f, -1.0f, 7.0f));
                REQUIRE(child.as_local_position(yoe::vector3_type(0.0f, 0.0f, 0.0f)) == yoe::vector3_type(-0.25f, 0.75f, -0.75f));
            }
        }
    }
}
