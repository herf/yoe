#pragma once
#include "object.hh"
#include "smart_pointer.hh"

namespace yoe
{
    class actor_type;

    class component_type: public object_type
    {
    public:
        using parent_type = smart_pointer_type<actor_type>;

        explicit component_type(parent_type parent);

        const parent_type& get_parent() const;

    protected:
        component_type() = default;

    private:
        friend class actor_type;

        void set_parent(parent_type parent)
        {
            _parent = std::move(parent);
        }

        parent_type _parent;
    };
}
