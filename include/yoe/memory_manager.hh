#pragma once
#include <type_traits>
#include <tuple>
#include <memory_manager/memory_manager.hh>

namespace yoe
{
    namespace details
    {
        //
        // integer sequence generation
        //

        template<int... integers>
        class sequence_type
        {

        };

        template<int i, int... integers>
        class sequence_generator_type: public sequence_generator_type<i - 1, i - 1, integers...>
        {

        };

        template<int... integers>
        class sequence_generator_type<0, integers...>
        {
        public:
            using type = sequence_type<integers...>;
        };

        //
        // get implementation
        //

        template<typename first_type, typename second_type>
        struct is_decay_same
        {
            static constexpr bool value = std::is_same<typename std::decay<first_type>::type, typename std::decay<second_type>::type>::value;
        };

        template<typename value_type, typename first_type, typename... value_types>
        typename std::enable_if<is_decay_same<value_type, first_type>::value, value_type&>::type get_helper(first_type& first, value_types&... values)
        {
            return first;
        }

        template<typename value_type, typename first_type, typename... value_types>
        typename std::enable_if<!is_decay_same<value_type, first_type>::value, value_type&>::type get_helper(first_type& first, value_types&... values)
        {
            return get_helper<value_type>(values...);
        }

        template<typename value_type, int... indices, typename... value_types>
        value_type& get_implementation(sequence_type<indices...> /*sequence*/, std::tuple<value_types...>& tuple)
        {
            return get_helper<value_type>(std::get<indices>(tuple)...);
        }
    }

    template<typename value_type, typename... value_types>
    value_type& get(std::tuple<value_types...>& tuple)
    {
        using concrete_sequence_type = typename details::sequence_generator_type<sizeof...(value_types)>::type;
        return details::get_implementation<value_type>(concrete_sequence_type(), tuple);
    }

    // Template class to manage memory_manager_types for each 'concrete_type'
    template<typename... concrete_types>
    class memory_manager_handler_type
    {
    public:
        memory_manager_handler_type();

        template<typename concrete_type>
        memory_manager_type& get()
        {
            using concrete_sequence_type = typename details::sequence_generator_type<sizeof...(concrete_types)>::type;
            return details::get_implementation<memory_manager_implementation_type<concrete_type>>(concrete_sequence_type(), _memory_managers);
        }

    private:
        template<typename concrete_type>
        class memory_manager_implementation_type: public memory_manager_type
        {
        public:
            using memory_manager_type::memory_manager_type;
        };

        std::tuple<memory_manager_implementation_type<concrete_types>...> _memory_managers;
    };
}

template <typename... concrete_types>
yoe::memory_manager_handler_type<concrete_types...>::memory_manager_handler_type():
    // Really messy code, but what is happening here is that we create a tuple base on the concrete_types parameter pack
    // Each element will be just a memory_manager_type, enhanced with the type from the parameter pack
    // With this, we can have different memory_manager_type objects for different types, like actor_type and so on...
    // TODO: What should be the value of the memory_block_size?
    _memory_managers(std::make_tuple(memory_manager_implementation_type<concrete_types>(memory_manager_type::settings_type(128 * sizeof(concrete_types)).set_minimum_free_block_size(sizeof(concrete_types)))...))
{
    // Nothing to do yet
}
