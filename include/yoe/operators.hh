#pragma once
#include "vector.hh"
#include "matrix.hh"

namespace yoe
{
    //
    // vector3_type operators
    //

    bool operator==(const vector3_type& left, const vector3_type& right);

    //
    // matrix44_type operators
    //

    matrix44_type operator*(const matrix44_type& left, const matrix44_type& right);
    vector4_type operator*(const matrix44_type& matrix, const vector4_type& vector);
}
