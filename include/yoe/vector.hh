#pragma once
#include <glm/glm.hpp>

namespace yoe
{
    class vector4_type;

    class vector3_type
    {
    public:
        explicit vector3_type(float xyz = 0.0f);
        vector3_type(float x, float y, float z);
        explicit vector3_type(const vector4_type& vector);

        bool is_equal(const vector3_type& other) const;

        float get_x() const;
        float get_y() const;
        float get_z() const;

        float distance(const vector3_type& other) const;

    private:
        glm::vec3 _vector;
    };

    class vector4_type
    {
    public:
        explicit vector4_type(float xyzw = 0.0f);
        vector4_type(float x, float y, float z, float w);
        vector4_type(const vector3_type& vector, float w);

        float get_x() const;
        float get_y() const;
        float get_z() const;
        float get_w() const;

    private:
        glm::vec4 _vector;
    };
}
