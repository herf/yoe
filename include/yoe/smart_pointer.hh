#pragma once
#include "object.hh"
#include  "unique_pointer.hh"

namespace yoe
{
    class world_type;

    template<typename value_type>
    class smart_pointer_type
    {
    public:
        smart_pointer_type() = default;

        smart_pointer_type(const smart_pointer_type& other): _control_block(other._control_block)
        {
            _control_block->increment_counter();
        }

        smart_pointer_type(smart_pointer_type&& other) noexcept: _control_block(other._control_block)
        {
            _control_block->increment_counter();
        }

        ~smart_pointer_type()
        {
            if(nullptr != _control_block)
                _control_block->decrement_counter();
        }

        bool is_valid() const
        {
            return nullptr != _control_block && _control_block->get_value() != nullptr;
        }

        void destroy();

        explicit operator bool() const
        {
            return is_valid();
        }

        bool operator==(const smart_pointer_type& other) const
        {
            return _control_block == other._control_block;
        }

        smart_pointer_type& operator=(const smart_pointer_type& other)
        {
            if(this != &other)
            {
                if(nullptr != _control_block)
                    _control_block->decrement_counter();
                _control_block = other._control_block;
                if(nullptr != _control_block)
                    _control_block->increment_counter();
            }

            return *this;
        }

        smart_pointer_type& operator=(smart_pointer_type&& other) noexcept
        {
            if(this != &other)
            {
                if(nullptr != _control_block)
                    _control_block->decrement_counter();
                _control_block = other._control_block;
                if(nullptr != _control_block)
                    _control_block->increment_counter();
            }

            return *this;
        }

        value_type* operator->()
        {
            return nullptr != _control_block ? static_cast<value_type*>(_control_block->get_value()) : nullptr;
        }

        const value_type* operator->() const
        {
            return nullptr != _control_block ? static_cast<value_type*>(_control_block->get_value()) : nullptr;
        }

        template<typename to_type>
        smart_pointer_type<to_type> cast()
        {
            static_assert(std::is_convertible<value_type*, to_type*>::value, "Incompatible smart_pointer_type objects!");

            return smart_pointer_type<to_type>(_control_block);
        }

    private:
        friend class world_type;
        template<typename any_type>
        friend class smart_pointer_type;
        template<typename to_type, typename from_type>
        friend smart_pointer_type<to_type> static_pointer_cast(const smart_pointer_type<from_type>& from);
        template<typename to_type, typename from_type>
        friend smart_pointer_type<to_type> dynamic_pointer_cast(const smart_pointer_type<from_type>& from);

        using parent_pointer_type = unique_pointer_type<object_type>;
        using control_block_type = parent_pointer_type::control_block_type;

        explicit smart_pointer_type(const parent_pointer_type& object):
            _control_block(object._control_block)
        {
            _control_block->increment_counter();
        }

        explicit smart_pointer_type(control_block_type* control_block):
            _control_block(control_block)
        {
            _control_block->increment_counter();
        }

        control_block_type* _control_block = nullptr;
    };

    template<typename to_type, typename from_type>
    smart_pointer_type<to_type> static_pointer_cast(const smart_pointer_type<from_type>& from)
    {
        static_assert(std::is_convertible<from_type*, to_type*>::value, "Incompatible smart_pointer_type objects!");

        return smart_pointer_type<to_type>(from._control_block);
    }

    template<typename to_type, typename from_type>
    smart_pointer_type<to_type> dynamic_pointer_cast(const smart_pointer_type<from_type>& from)
    {
        if(nullptr != dynamic_cast<to_type*>(from._control_block->get_value()))
        {
            return smart_pointer_type<to_type>(from._control_block);
        }

        return {};
    }
}

#include "world.hh"

template <typename value_type>
void yoe::smart_pointer_type<value_type>::destroy()
{
    if(is_valid())
        world_type::get_instance().destroy(cast<object_type>());
}
