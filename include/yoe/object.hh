#pragma once
#include <cstddef>

class memory_manager_type;

namespace yoe
{
    class world_type;

    class object_type
    {
    public:
        class deleter_type
        {
        public:
            explicit deleter_type(memory_manager_type& memory_manager);

            void operator()(object_type* object) const;

        private:
            memory_manager_type& _memory_manager;
        };

        object_type() = default;
        object_type(const object_type&) = default;
        object_type(object_type&&) = default;
        virtual ~object_type() = default;

        object_type& operator=(const object_type&) = default;
        object_type& operator=(object_type&&) = default;

        void operator delete(void* pointer, memory_manager_type& memory_manager);
        void operator delete(void* pointer);

    private:
        friend class world_type;

        void* operator new(size_t size, memory_manager_type& memory_manager);
    };
}
