#pragma once
#include "vector.hh"
#include "matrix.hh"

namespace yoe
{
    class transform_type
    {
    public:
        explicit transform_type(vector3_type position = vector3_type(0.0f),
                                vector3_type scale = vector3_type(1.0f),
                                vector3_type rotation = vector3_type(0.0f),
                                const transform_type* parent = nullptr);
        const vector3_type& get_local_position() const;
        vector3_type get_world_position() const;
        vector3_type as_local_position(const vector3_type& world_position) const;
        vector3_type as_world_position(const vector3_type& local_position) const;
        matrix44_type get_matrix() const;

        void set_parent(const transform_type* parent);

    private:
        vector3_type _position;
        vector3_type _scale;
        vector3_type _rotation;
        const transform_type* _parent;
    };
}
