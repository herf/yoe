#pragma once
#include <vector>
#include "object.hh"
#include "smart_pointer.hh"
#include "world.hh"

namespace yoe
{
    class component_type;

    class actor_type: public object_type
    {
    public:
        template<typename value_type>
        using container_type = std::vector<value_type>;
        using component_pointer_type = smart_pointer_type<component_type>;
        using components_type = container_type<component_pointer_type>;

        actor_type() = default;
        actor_type(const actor_type&) = delete;
        actor_type(actor_type&&) = delete;
        ~actor_type();

        actor_type& operator=(const actor_type&) = delete;
        actor_type& operator=(actor_type&&) = delete;

        template<typename concrete_type, typename... argument_types>
        smart_pointer_type<concrete_type> create_component(argument_types&&... arguments)
        {
            static_assert(std::is_base_of<component_type, concrete_type>::value, "Only descendants of component_type can be created");

            smart_pointer_type<concrete_type> result = world_type::get_instance().create<concrete_type>(std::forward<argument_types>(arguments)...);

            result->set_parent(world_type::get_instance().get_smart_pointer(this));
            _components.push_back(result.template cast<component_type>());

            return result;
        }

        template<typename concrete_type>
        smart_pointer_type<concrete_type> get_component() const
        {
            for(const components_type::value_type& component: _components)
            {
                smart_pointer_type<concrete_type> result = dynamic_pointer_cast<concrete_type>(component);

                if(result)
                {
                    return result;
                }
            }

            return {};
        }

        const components_type& get_components() const
        {
            return _components;
        }

        template<typename concrete_type>
        container_type<smart_pointer_type<concrete_type>> get_components() const
        {
            using pointer_type = smart_pointer_type<concrete_type>;
            container_type<pointer_type> result;

            result.reserve(_components.size());
            for(const components_type::value_type& component: _components)
            {
                pointer_type concrete_component = dynamic_pointer_cast<concrete_type>(component);

                if(concrete_component)
                {
                    result.push_back(std::move(concrete_component));
                }
            }

            return result;
        }

    private:
        components_type _components;
    };
}
