#pragma once
#include <glm/glm.hpp>
#include "vector.hh"

namespace yoe
{
    class matrix44_type
    {
    public:
        explicit matrix44_type(float value = 1.0f);

        matrix44_type get_inverse() const;
        void inverse();

        matrix44_type get_translate(const vector3_type& position) const;
        void translate(const vector3_type& position);

        matrix44_type get_scale(const vector3_type& scale) const;
        void scale(const vector3_type& scale);

        matrix44_type get_rotate(float angle, const vector3_type& axis) const;
        void rotate(float angle, const vector3_type& axis);

        matrix44_type multiply(const matrix44_type& other) const;
        vector4_type multiply(const vector4_type& vector) const;
    
    private:
        explicit matrix44_type(glm::mat4 matrix);
        glm::mat4 _matrix;
    };
}
