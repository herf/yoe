#pragma once
#include <type_traits>
#include <memory>
#include <vector>
#include <algorithm>
#include <tuple>
#include <memory_manager/memory_manager.hh>
#include "object.hh"
#include "unique_pointer.hh"
#include "memory_manager.hh"

namespace yoe
{
    class actor_type;

    class world_type
    {
    public:
        static world_type& get_instance();

        world_type(const world_type&) = delete;
        world_type(world_type&&) = delete;
        world_type& operator=(const world_type&) = delete;
        world_type& operator=(world_type&&) = delete;

        template<typename concrete_type, typename... argument_types>
        smart_pointer_type<concrete_type> create(argument_types&&... arguments);

        template<typename concrete_type>
        smart_pointer_type<concrete_type> get_smart_pointer(concrete_type* pointer);

        void destroy(const smart_pointer_type<object_type>& object);

    private:
        using object_pointer_type = unique_pointer_type<object_type>;
        using objects_type = std::vector<object_pointer_type>;

        world_type() = default;
        ~world_type();

        objects_type _objects;
        bool _teardown = false;
        memory_manager_handler_type<actor_type, object_type> _memory_manager_handler;
    };
}

#include "smart_pointer.hh"
#include "actor.hh"

template <typename concrete_type, typename... argument_types>
yoe::smart_pointer_type<concrete_type> yoe::world_type::create(argument_types&&... arguments)
{
    static_assert(std::is_base_of<object_type, concrete_type>::value, "Only descendants of object_type can be used in world");
    concrete_type* object = nullptr;

    if(std::is_base_of<actor_type, concrete_type>::value)
    {
        memory_manager_type& memory_manager = _memory_manager_handler.get<actor_type>();

        object = new (memory_manager) concrete_type(std::forward<argument_types>(arguments)...);
        _objects.push_back(object_pointer_type(object, object_type::deleter_type(memory_manager)));
    }
    // TODO: Add other types as needed
    else
    {
        memory_manager_type& memory_manager = _memory_manager_handler.get<object_type>();

        object = new (memory_manager) concrete_type(std::forward<argument_types>(arguments)...);
        _objects.push_back(object_pointer_type(object, object_type::deleter_type(memory_manager)));
    }

    return smart_pointer_type<concrete_type>(_objects.back());
}

template <typename concrete_type>
yoe::smart_pointer_type<concrete_type> yoe::world_type::get_smart_pointer(concrete_type* pointer)
{
    static_assert(std::is_base_of<object_type, concrete_type>::value, "Only descendants of object_type can be used in world");
    auto predicate = [pointer](const objects_type::value_type& value)
    {
        return value._control_block->get_value() == pointer;
    };
    const objects_type::const_iterator iterator = std::find_if(_objects.begin(), _objects.end(), predicate);

    if (iterator != _objects.end())
    {
        return smart_pointer_type<concrete_type>(*iterator);
    }

    return {};
}
