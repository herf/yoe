#pragma once
#include <cstdint>

namespace yoe
{
    class world_type;
    template<typename value_type>
    class smart_pointer_type;

    template<typename value_type>
    class unique_pointer_type
    {
    public:
        unique_pointer_type(const unique_pointer_type&) = delete;
        unique_pointer_type(unique_pointer_type&& other) noexcept:
            _control_block(other._control_block),
            _deleter(other._deleter)
        {
            other._control_block = nullptr;
            other._deleter = nullptr;
        }

        ~unique_pointer_type()
        {
            if(nullptr != _deleter)
            {
                _deleter->call();
                _deleter = nullptr;
            }
            if(nullptr != _control_block)
            {
                _control_block->clear_value();
                _control_block = nullptr;
            }
        }

        unique_pointer_type& operator=(const unique_pointer_type&) = delete;
        unique_pointer_type& operator=(unique_pointer_type&& other) noexcept
        {
            if(nullptr != _deleter)
                _deleter->call();
            if(nullptr != _control_block)
                _control_block->clear_value();
            _deleter = other._deleter;
            _control_block = other._control_block;
            other._deleter = nullptr;
            other._control_block = nullptr;

            return *this;
        }

    private:
        friend class world_type;
        template<typename any_type>
        friend class smart_pointer_type;

        template<typename deleter_type>
        explicit unique_pointer_type(value_type* value, const deleter_type& deleter):
            _control_block(new control_block_type(value)),
            _deleter(new deleter_implementation_type<deleter_type>(_control_block, deleter))
        {
            // Nothing to do yet
        }

        class control_block_type
        {
        public:
            explicit control_block_type(value_type* value):
                _value(value)
            {
                // Nothing to do yet
            }

            value_type* get_value()
            {
                return _value;
            }

            const value_type* get_value() const
            {
                return _value;
            }

            void clear_value()
            {
                _value = nullptr;
                cleanup();
            }

            void increment_counter()
            {
                ++_counter;
            }

            void decrement_counter()
            {
                --_counter;
                cleanup();
            }

        private:
            void cleanup()
            {
                if(0 == _counter && nullptr == _value)
                {
                    delete this;
                }
            }

            value_type* _value = nullptr;
            uint32_t _counter = 0;
        };

         class deleter_base_type
        {
        public:
            deleter_base_type() = default;
            deleter_base_type(const deleter_base_type&) = default;
            deleter_base_type(deleter_base_type&&) = default;
            virtual ~deleter_base_type() = default;

            deleter_base_type& operator=(const deleter_base_type&) = default;
            deleter_base_type& operator=(deleter_base_type&&) = default;

            virtual void call() = 0;
            
        };

        template<typename implementation_type>
        class deleter_implementation_type: public deleter_base_type
        {
        public:
            explicit deleter_implementation_type(control_block_type* control_block, const implementation_type& implementation):
                _control_block(control_block),
                _implementation(implementation)
            {
                // Nothing to do yet
            }

            virtual void call() override
            {
                _implementation(_control_block->get_value());
            }

        private:
            control_block_type* _control_block = nullptr;
            implementation_type _implementation;
        };

        control_block_type* _control_block = nullptr;
        deleter_base_type* _deleter = nullptr;
    };
}
