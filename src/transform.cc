#include <yoe/transform.hh>
#include <yoe/operators.hh>

namespace yoe
{
    transform_type::transform_type(const vector3_type position,
                                   const vector3_type scale,
                                   const vector3_type rotation,
                                   const transform_type* parent):
        _position(position),
        _scale(scale),
        _rotation(rotation),
        _parent(parent)
    {
        // Nothing to do yet
    }

    const vector3_type& transform_type::get_local_position() const
    {
        return _position;
    }

    vector3_type transform_type::get_world_position() const
    {
        if (nullptr == _parent)
        {
            return get_local_position();
        }

        return vector3_type(_parent->get_matrix() * vector4_type(_position, 1.0f));
    }

    vector3_type transform_type::as_local_position(const vector3_type& world_position) const
    {
        const matrix44_type inverse_matrix = get_matrix().get_inverse();

        return vector3_type(inverse_matrix * vector4_type(world_position, 1.0f));
    }

    vector3_type transform_type::as_world_position(const vector3_type& local_position) const
    {
        return vector3_type(get_matrix() * vector4_type(local_position, 1.0f));
    }

    matrix44_type transform_type::get_matrix() const
    {
        matrix44_type model(1.0f);

        model.translate(_position);
        model.scale(_scale);
        model.rotate(_rotation.get_x(), vector3_type(1.0f, 0.0f, 0.0f));
        model.rotate(_rotation.get_y(), vector3_type(0.0f, 1.0f, 0.0f));
        model.rotate(_rotation.get_z(), vector3_type(0.0f, 0.0f, 1.0f));

        if (nullptr !=_parent)
        {
            model = _parent->get_matrix() * model;
        }

        return model;
    }

    void transform_type::set_parent(const transform_type* parent)
    {
        _parent = parent;
    }
}
