#include <yoe/matrix.hh>
#include <glm/gtc/matrix_transform.hpp>

namespace yoe
{
    matrix44_type::matrix44_type(const float value): _matrix(value)
    {
        // Nothing to do yet
    }

    matrix44_type matrix44_type::get_inverse() const
    {
        return matrix44_type(glm::inverse(_matrix));
    }

    void matrix44_type::inverse()
    {
        _matrix = glm::inverse(_matrix);
    }

    matrix44_type matrix44_type::get_translate(const vector3_type& position) const
    {
        return matrix44_type(glm::translate(_matrix, glm::vec3(position.get_x(), position.get_y(), position.get_z())));
    }

    void matrix44_type::translate(const vector3_type& position)
    {
        _matrix = glm::translate(_matrix, glm::vec3(position.get_x(), position.get_y(), position.get_z()));
    }

    matrix44_type matrix44_type::get_scale(const vector3_type& scale) const
    {
        return matrix44_type(glm::scale(_matrix, glm::vec3(scale.get_x(), scale.get_y(), scale.get_z())));
    }

    void matrix44_type::scale(const vector3_type& scale)
    {
        _matrix = glm::scale(_matrix, glm::vec3(scale.get_x(), scale.get_y(), scale.get_z()));
    }

    matrix44_type matrix44_type::get_rotate(float angle, const vector3_type& axis) const
    {
        return matrix44_type(glm::rotate(_matrix, glm::radians(angle), glm::vec3(axis.get_x(), axis.get_y(), axis.get_z())));
    }

    void matrix44_type::rotate(const float angle, const vector3_type& axis)
    {
        _matrix = glm::rotate(_matrix, glm::radians(angle), glm::vec3(axis.get_x(), axis.get_y(), axis.get_z()));
    }

    matrix44_type matrix44_type::multiply(const matrix44_type& other) const
    {
        return matrix44_type(_matrix * other._matrix);
    }

    vector4_type matrix44_type::multiply(const vector4_type& vector) const
    {
        const glm::vec4 result = _matrix * glm::vec4(vector.get_x(), vector.get_y(), vector.get_z(), vector.get_w());

        return vector4_type(result.x, result.y, result.z, result.w);
    }

    matrix44_type::matrix44_type(const glm::mat4 matrix): _matrix(matrix)
    {
        // Nothing to do yet
    }
}
