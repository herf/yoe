#include <yoe/actor.hh>
#include <yoe/component.hh>

yoe::actor_type::~actor_type()
{
    for(components_type::value_type& component: _components)
    {
        component.destroy();
    }
    _components.clear();
}
