#include <utility>
#include <yoe/component.hh>

namespace yoe
{
    component_type::component_type(parent_type parent): _parent(std::move(parent))
    {
        // Nothing to do yet
    }

    const component_type::parent_type& component_type::get_parent() const
    {
        return _parent;
    }
}
