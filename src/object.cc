#include <cstdlib>
#include <yoe/object.hh>
#include <memory_manager/memory_manager.hh>

namespace yoe
{
    object_type::deleter_type::deleter_type(memory_manager_type& memory_manager): _memory_manager(memory_manager)
    {
        // Nothing to do yet
    }

    void object_type::deleter_type::operator()(object_type* object) const
    {
        delete object;
        _memory_manager.deallocate(object);
        // object->operator delete(object, _memory_manager);
    }

    void* yoe::object_type::operator new(size_t size, memory_manager_type& memory_manager)
    {
        return memory_manager.allocate(static_cast<memory_manager_type::size_type>(size));
    }

    void object_type::operator delete(void* pointer, memory_manager_type& memory_manager)
    {
        memory_manager.deallocate(pointer);
    }

    void object_type::operator delete(void* /*pointer*/)
    {
        // Nothing to do yet, pointer is already deallocated in memory_manager
        // at deleter_type::operator()
    }
}
