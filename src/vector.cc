#include <yoe/vector.hh>

namespace yoe
{
    //
    // vector3_type
    //

    vector3_type::vector3_type(const float xyz): vector3_type(xyz, xyz, xyz)
    {
        // Nothing to do yet
    }

    vector3_type::vector3_type(const float x, const float y, const float z): _vector(x, y, z)
    {
        // Nothing to do yet
    }

    vector3_type::vector3_type(const vector4_type& vector):
        vector3_type(vector.get_x(), vector.get_y(), vector.get_z())
    {
        // Nothing to do yet
    }

    bool vector3_type::is_equal(const vector3_type& other) const
    {
        return _vector == other._vector;
    }

    float vector3_type::get_x() const
    {
        return _vector.x;
    }

    float vector3_type::get_y() const
    {
        return _vector.y;
    }

    float vector3_type::get_z() const
    {
        return _vector.z;
    }

    float vector3_type::distance(const vector3_type& other) const
    {
        return glm::distance(_vector, other._vector);
    }

    //
    // vector4_type
    //

    vector4_type::vector4_type(const float xyzw): vector4_type(xyzw, xyzw, xyzw, xyzw)
    {
        // Nothing to do yet
    }

    vector4_type::vector4_type(const float x, const float y, const float z, const float w): _vector(x, y, z, w)
    {
        // Nothing to do yet
    }

    vector4_type::vector4_type(const vector3_type& vector, const float w):
        vector4_type(vector.get_x(), vector.get_y(), vector.get_z(), w)
    {
        // Nothing to do yet
    }

    float vector4_type::get_x() const
    {
        return _vector.x;
    }

    float vector4_type::get_y() const
    {
        return _vector.y;
    }

    float vector4_type::get_z() const
    {
        return _vector.z;
    }

    float vector4_type::get_w() const
    {
        return _vector.w;
    }
}
