#include <yoe/world.hh>

namespace yoe
{
    world_type& world_type::get_instance()
    {
        static world_type world;

        return world;
    }

    void world_type::destroy(const smart_pointer_type<object_type>& object)
    {
        if(!_teardown)
        {
            auto predicate = [object](const objects_type::value_type& value)
            {
                return value._control_block->get_value() == object._control_block->get_value();
            };
            const objects_type::const_iterator iterator = std::find_if(_objects.begin(), _objects.end(), predicate);

            if(iterator != _objects.end())
            {
                _objects.erase(iterator);
            }
        }
    }

    world_type::~world_type()
    {
        _teardown = true;
        _objects.clear();
    }
}
